/* ADVANCED JAVASCRIPT OOP - INHERITANCE (ACTIVITY) */

/*
	1. Create a new class called Contractor
		- A contractor has the following properties: name, email, contactNo
		- A contractor also has a method called getContractorDetails and displays the name, email, and contactNo of the contractor
		- Test Output:
		let contractor1= new Contractor("Ultra Manpower Services", "ultra@manpower.com", "09167890123");
		contractor1.getContractorDetails();
		- Expected Output:
		Name: Ultra Manpower Services
		Email: ultra@power.com
		Contact No: 09167890123
	2. Create a new class called Subcontractor that inherits the Contractor
		- A subcontractor has an additional property called specializations which is an array of specializations as strings
		- A subcontractor has a method called getSubConDetails that displays all the information of the subcontractor
		- Test Output:
		let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "09151234567", ["gardens", "industrial"]);
		let subCont2 = new Subcontractor("LitC corp", "litc@mail.com", "091512456789", ["schools", "hospitals", "bakeries", "libraries"]);
		- Expected Output:
		Name: Ace Bros
		Email: acebros@mail.com
		Contact No: 09151234567
		gardens,industrial
		Name: LitC corp
		Email: litc@mail.com
		Contact No: 091512456789
		schools,hospitals,bakeries,libraries
*/
class Contractor {
	constructor(name, email, contactNo) {
		this.name = name;
		this.email = email;
		this.contactNo = contactNo;
	}
	getContractorDetails() {
		console.log(`Name: ${this.name}`);
		console.log(`Email: ${this.email}`);
		console.log(`Contact No: ${this.contactNo}`);
	}
}
let contractor1= new Contractor("Ultra Manpower Services", "ultra@manpower.com", "09167890123");
contractor1.getContractorDetails();

class Subcontractor extends Contractor {
	constructor(name, email, contactNo, specializations) {
		super(name, email, contactNo);
		this.specializations = specializations;
	}
	getSubConDetails() {
		console.log((this.specializations).toString());
	}
}
let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "09151234567", ["gardens", "industrial"]);
let subCont2 = new Subcontractor("LitC corp", "litc@mail.com", "091512456789", ["schools", "hospitals", "bakeries", "libraries"]);
subCont1.getContractorDetails();
subCont1.getSubConDetails();
subCont2.getContractorDetails();
subCont2.getSubConDetails();