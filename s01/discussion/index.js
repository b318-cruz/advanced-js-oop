/* ADVANCED JAVASCRIPT OOP - INHERITANCE */

/*
	Inheritance
	- It refers to th mechanism by which one object can inherit properties and methods from another class.
*/
class Person {
	constructor(firstName, lastName) {
		// property = value using parameter

		// properties
		this.firstName = firstName;
		this.lastName = lastName;
	}

	// methods
	getFullName() {
		return `${this.firstName} ${this.lastName}`;
	}
}

// instantiate a new Person object
const person1 = new Person("John", "Smith");
console.log(person1);
console.log(person1.getFullName());

const person2 = new Person("John", "Doe");
console.log(person2.getFullName());

// child class
/*
	The extends keywords in JavaScript is used to establish an inheritance relationship between classes. It allows properties and methods from a parent class to the child class. "extends" keyword states that the Employee class is a child of the Person class.
*/
class Employee extends Person {
	constructor(employeeId, firstName, lastName) {
		super(firstName, lastName);
		/*
			The super constructor can have parameter/s that matches the constructor of the parent.

			By doing this, we don't need to manually assign values to the inherited properties.
		*/
		this.employeeId = employeeId;
	}

	getEmployeeDetails() {
		// return `The ID ${this.employeeId} belongs to ${this.firstName} ${this.lastName}`;

		// alternative approach
		return `The ID ${this.employeeId} belongs to ${this.getFullName()}`;		
	}
}

let employee1 = new Employee("Acme-001", "John", "Roberts");
console.log(employee1.getFullName());
console.log(employee1.getEmployeeDetails());

// child class extension
class TeamLead extends Employee {
	constructor(employeeId, firstName, lastName) {
		super(employeeId, firstName, lastName);
		this.teamMembers = [];
	}

	addTeamMember(employee) {
		this.teamMembers.push(employee);
		return this;
	}

	getTeamMembers() {
		this.teamMembers.forEach(member => {
			console.log(`${member.getFullName()}`);
		});
		return this;
	}
}
const teamLead = new TeamLead("Acme-002", "Leri", "Medina");
console.log(teamLead);
console.log(teamLead.getEmployeeDetails());

let employee2 = new Employee("Acme-003", "Brandon", "Smith");
let employee3 = new Employee("Acme-004", "Jobert", "Pakundangan");
let employee4 = new Employee("Acme-005", "Jhun Jhun", "Dela Cruz");
//console.log();
teamLead.addTeamMember(employee1);
teamLead.addTeamMember(employee2);
teamLead.addTeamMember(employee3);
teamLead.addTeamMember(employee4);
teamLead.getTeamMembers();

/*
	QUESTION: What are the properties and methods of TeamLead class?
	
	ANSWER:
	properties: employeeId, firstName, lastName, teamMembers
	methods: getFullName(), getEmployeeDetails(), addTeamMember(), getTeamMembers()
*/

/*
	QUESTION: Can we have 2 parent classes in inheritance?
	ANSWER:
	No, because JavaScript does not support multiple inheritance with classes.
*/