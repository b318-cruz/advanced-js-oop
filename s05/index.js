/* ADVANCED JAVASCRIPT OOP - CAPSTONE PROJECT */

// Lone Class (for Employee Child Class)
class Request {
	constructor(requesterEmail, content, dateRequested) {
		this.requesterEmail = requesterEmail;
		this.content = content;
		this.dateRequested = new Date();
	}
}

// Parent Class (Abstract)
class Person {

	constructor() {
		// constructor properties
		if(this.constructor === Person) {
			throw new Error("Object cannot be created from an abstract class Person.");
		}

		// required methods
		if(this.getFullName === undefined) {
			throw new Error("Class must implement 'getFullName()' method.");
		}
		if(this.login === undefined) {
			throw new Error("Class must implement 'login()' method.");
		}
		if(this.logout === undefined) {
			throw new Error("Class must implement 'logout()' method.");
		}
	}
}

// Child Class #1
class Employee extends Person {
	// private properties
	#firstName; #lastName; #email; #department; #isActive; #requests;

	// constructor
	constructor(firstName, lastName, email, department, isActive = true) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = isActive;
		this.#requests = [];
	}

	// methods
	getFullName() {
		return this.getFirstName() + " " + this.getLastName();
	}
	login() {
		return this.getFullName() + " has logged in.";
	}
	logout() {
		return this.getFullName() + " has logged out.";
	}
	addRequest(content) {
	    const request = new Request(this.#email, content);
	    this.#requests.push(request);
	}

	// getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getIsActive() {
		return this.#isActive;
	}
	getRequests() {
		return this.#requests;
	}

	// setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
	setIsActive(isActive) {
		this.#isActive = isActive;
	}
}

// Child Class #2
class TeamLead extends Person {
	// private properties
	#firstName; #lastName; #email; #department; #isActive; #members;

	// constructor
	constructor(firstName, lastName, email, department, isActive = true) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = isActive;
		this.#members = [];
	}

	// methods
	getFullName() {
		return this.getFirstName() + " " + this.getLastName();
	}
	login() {
		return "Team Lead " + this.getFullName() + " has logged in.";
	}
	logout() {
		return "Team Lead " + this.getFullName() + " has logged out.";
	}
	addMember(employee) {
	    this.#members.push(employee);
	}
	checkRequests(employeeEmail) {

		const employeeIndex = this.#members.findIndex(
			(member) => {
				return member.getEmail() === employeeEmail
			}
		);

	    if (employeeIndex !== -1) {
	    	const employee = this.#members[employeeIndex];
	    	return employee.getRequests();
	    } else {
	    	return "Employee not found";
	    }
	}

	// getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getIsActive() {
		return this.#isActive;
	}
	getMembers() {
		return this.#members;
	}

	// setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
	setIsActive(isActive) {
		this.#isActive = isActive;
	}
}

// Child Class #3
class Admin extends Person {
	// private properties
	#firstName; #lastName; #email; #department; #isActive; #teamLeads;

	// constructor
	constructor(firstName, lastName, email, department) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#teamLeads = [];
	}

	// methods
	getFullName() {
		return this.getFirstName() + " " + this.getLastName();
	}
	login() {
		return "Admin " + this.getFullName() + " has logged in.";
	}
	logout() {
		return "Admin " + this.getFullName() + " has logged out.";
	}
	addTeamLead(teamLead) {
	    this.#teamLeads.push(teamLead);
	}
	deactivateTeam(teamLeadEmail) {

		const teamLeadIndex = this.#teamLeads.findIndex(
			(lead) => {
				return lead.getEmail() === teamLeadEmail
			}
		);

	    if (teamLeadIndex !== -1) {
	      const teamLead = this.#teamLeads[teamLeadIndex];
	      teamLead.setIsActive(false);
	      teamLead.getMembers().forEach((member) => (member.setIsActive(false)));
	      this.#teamLeads.splice(teamLeadIndex, 1);
	      return "Team deactivated successfully.";
	    } else {
	      return "Team lead not found.";
	    }
	}

	// getters
	getFirstName() {
		return this.#firstName;
	}
	getLastName() {
		return this.#lastName;
	}
	getEmail() {
		return this.#email;
	}
	getDepartment() {
		return this.#department;
	}
	getTeamLeads() {
		return this.#teamLeads;
	}

	// setters
	setFirstName(firstName) {
		this.#firstName = firstName;
	}
	setLastName(lastName) {
		this.#lastName = lastName;
	}
	setEmail(email) {
		this.#email = email;
	}
	setDepartment(department) {
		this.#department = department;
	}
}

// Invocations
const john = new Employee("John", "Doe", "john.doe@example.com", "IT");
const sarah = new Employee("Sarah", "Smith", "sarah.smith@example.com", "IT");
const mike = new TeamLead("Mike", "Johnson", "mike.johnson@example.com", "IT");
const admin = new Admin("Adam", "Mineral", "admin@example.com", "Administration");

john.addRequest("Please approve my vacation request.");
sarah.addRequest("Can I work remotely next week?");

mike.addMember(john);
mike.addMember(sarah);

admin.addTeamLead(mike);

console.log(john.getFullName());
console.log(john.login());
console.log(john.getRequests());
console.log(john.logout());

console.log(sarah.getFullName());
console.log(sarah.login());
console.log(sarah.getRequests());
console.log(sarah.logout());

console.log(mike.getFullName());
console.log(mike.login());
console.log(mike.getMembers());
console.log(mike.checkRequests(john.getEmail()));
console.log(mike.logout());

console.log(admin.getFullName());
console.log(admin.login());
console.log(admin.getTeamLeads());
console.log(admin.deactivateTeam(mike.getEmail()));
console.log(admin.logout());

console.log(john.getIsActive());
console.log(sarah.getIsActive());
console.log(mike.getIsActive());