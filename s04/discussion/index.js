/* ADVANCED JAVASCRIPT OOP - ENCAPSULATION */

// What is Encapsulation
/*
	Encapsulation refers to the grouping of data, along with the methods that operate on those data, into a single unit.

	Abstraction and encapsulation are complementary concepts: abstraction focuses on the observable behavior of an object. Encapsulation forces on the implementation that gives rise to this behavior.

	Control access of data in encapsulation (properties and methods are in 'private') is with the use of accessor methods in the form of getters and setters. 	
*/
class Person {
	constructor() {
		if(this.constructor === Person) {
			throw new Error(
				"Object cannot be created from an abstract class Person."
			);
		}

		if(this.getFullName === undefined) {
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
	}
}

class Employee extends Person {
	// Encapsulation, aided by private fields (#), ensures data protection. Setters and getters provide controlled access to encapsulated data.

	// private fields
	#firstName;
	#lastName;
	#employeeID;
	#password;

	constructor(firstName, lastName, employeeID, password) {
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeID = employeeID;
		this.#password = password;
	}

	// getter methods
	getFirstName() {
		return `First Name: ${this.#firstName}`;
	}

	getLastName() {
		return `Last Name: ${this.#lastName}`;
	}

	getEmployeeId() {
		return `Employee ID: ${this.#employeeID}`;
	}

	getFullName() {
		return `${this.#firstName} ${this.#lastName} has an employee ID of ${this.#employeeID}.`;
	}

	// setter methods
	setFirstName(firstName) {
		this.#firstName = firstName;
	}

	setLastName(lastName) {
		this.#lastName = lastName;
	}

	setEmployeeId(employeeID) {
		if(this.#password === undefined) {
			throw new Error(
				"You are not authorized to use this function"
			);
		} else {
			this.#employeeID = employeeID;
		}
	}

	inputPassword(providedPassword) {
		if(providedPassword === "access1001") {
			this.#password = providedPassword;
		} else {
			throw new Error(
				"Wrong password"
			);
		}
	}
}

/*const employeeA = new Employee('John', 'Smith', 'EM-001');

// direct access with the field/property firstName will return undefined because the property is private
console.log(employeeA.firstName);

// We can't directly change the value of the property firstName because the property is private.
employeeA.firstName = 'David';

console.log(employeeA.getFirstName());

employeeA.setFirstName('David');
console.log(employeeA.getFirstName());
console.log(employeeA.getFullName());

const employeeB = new Employee();
console.log(employeeB.getFullName());

employeeB.setFirstName('Jill');
employeeB.setLastName('Hill');
employeeB.setEmployeeId('EM-002');

console.log(employeeB.firstName);
employeeB.#firstName = 'Jack';

console.log(employeeB.getFullName());*/

const employeeC = new Employee();
// employeeC.password = 'access1001';
employeeC.inputPassword('access1001');
employeeC.setEmployeeId('EM-003');
console.log(employeeC.getEmployeeId());