/* ADVANCED JAVASCRIPT OOP - ENCAPSULATION (ACTIVITY) */

/* ACTIVITY 1 */
// Base class
class RegularShape {
	constructor() {
		// properties
		if(this.constructor === RegularShape) {
			throw new Error(
				"Object cannot be created from an abstract class RegularShape."
			);
		}
		//methods
		if(this.getPerimeter === undefined) {
			throw new Error(
				"Class must implement 'getPerimeter()' method."
			);
		}
		if(this.getArea === undefined) {
			throw new Error(
				"Class must implement 'getArea()' method."
			);
		}
	}
}
// 1st subclass
class Square extends RegularShape {
	// private fields
	#noSides; #length;
	// constructor
	constructor(noSides, length) {
		super();
		this.#noSides = noSides;
		this.#length = length;
	}
	// methods
	getPerimeter() {
		if (this.#noSides === 4) {
		   return "The perimeter of a square is " + (this.getNoSides() * this.getLength()) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
	getArea() {
		if (this.#noSides === 4) {
		    return "The area of a square is " + (this.getLength() * this.getLength()) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
	// getters
	getNoSides() {
		return this.#noSides;
	}
	getLength() {
		return this.#length;
	}
	// setters
	setNoSides(noSides) {
		this.#noSides = noSides;
	}
	setLength(length) {
		this.#length = length;
	}
}
let square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength());
console.log(square1.getPerimeter());
// 2nd subclass
class Triangle extends RegularShape {
	// private fields
	#noSides; #length;
	// constructor
	constructor(noSides, length) {
		super();
		this.#noSides = noSides;
		this.#length = length;
	}
	// methods
	getPerimeter() {
		if (this.#noSides === 3) {
		    return "The perimeter of a square is " + (this.getNoSides() * this.getLength()) + ".";
		} else {
			return "Invalid input! A triangle has 3 sides.";
		}
	}
	getArea() {
		if (this.#noSides === 3) {
		    return "The area of a square is " + ((Math.sqrt(3) / 4) * Math.pow(this.getLength(), 2)).toFixed(3) + ".";
		} else {
			return "Invalid input! A triangle has 3 sides.";
		}
	}
	// getters
	getNoSides() {
		return this.#noSides;
	}
	getLength() {
		return this.#length;
	}
	// setters
	setNoSides(noSides) {
		this.#noSides = noSides;
	}
	setLength(length) {
		this.#length = length;
	}
}
let triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength());
console.log(triangle1.getArea());

/* ACTIVITY 2 */
// Base class
class User {
	constructor() {
		// properties
		if(this.constructor === User) {
			throw new Error(
				"Object cannot be created from an abstract class User."
			);
		}
		// methods
		if(this.login === undefined) {
			throw new Error(
				"Class must implement 'login()' method."
			);
		}
		if(this.register === undefined) {
			throw new Error(
				"Class must implement 'register()' method."
			);
		}
		if(this.logout === undefined) {
			throw new Error(
				"Class must implement 'logout()' method."
			);
		}
	}
}
// 1st subclass
class RegularUser extends User {
	// private fields
	#name; #email; #password;
	// constructor
	constructor(name, email, password) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
	}
	// methods
	login() {
		return `${this.#name} has logged in.`;
	}
	register() {
		return `${this.#name} has registered.`;
	}
	logout() {
		return `${this.#name} has logged out.`;
	}
	browseJob() {
		return 'There are 10 jobs found.';
	}
	// getters
	getName() {
		return this.#name;
	}
	getEmail() {
		return this.#email;
	}
	getPassword() {
		return this.#password;
	}
	// setters
	setName(name) {
		this.#name = name;
	}
	setEmail(email) {
		this.#email = email;
	}
	setPassword(password) {
		this.#password = password;
	}
}
const regUser1 = new RegularUser();
regUser1.setName('Dan');
regUser1.setEmail('dan@mail.com');
regUser1.setPassword('Dan12345');
console.log(regUser1.register());
console.log(regUser1.login());
console.log(regUser1.browseJob());
console.log(regUser1.logout());
// 2nd subclass
class Admin extends User {
	// private fields
	#name; #email; #password; #hasAdminExpired;
	// constructor
	constructor(name, email, password, hasAdminExpired) {
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
		this.#hasAdminExpired = hasAdminExpired;
	}
	// methods
	login() {
		return `Admin ${this.#name} has logged in.`;
	}
	register() {
		return `Admin ${this.#name} has registered.`;
	}
	logout() {
		return `Admin ${this.#name} has logged out.`;
	}
	postJob() {
		return 'Job posting added to site.';
	}	
	// getters
	getName() {
		return this.#name;
	}
	getEmail() {
		return this.#email;
	}
	getPassword() {
		return this.#password;
	}
	getHasAdminExpired() {
		return this.#hasAdminExpired;
	}
	// setters
	setName(name) {
		this.#name = name;
	}
	setEmail(email) {
		this.#email = email;
	}
	setPassword(password) {
		this.#password = password;
	}
	setHasAdminExpired(hasAdminExpired) {
		this.#hasAdminExpired = hasAdminExpired;
	}
}
const admin = new Admin();
admin.setName('Joe');
admin.setEmail('admin_joe@mail.com');
admin.setPassword('joe12345');
admin.setHasAdminExpired(false);
console.log(admin.register());
console.log(admin.login());
console.log(admin.getHasAdminExpired());
console.log(admin.postJob());
console.log(admin.logout());