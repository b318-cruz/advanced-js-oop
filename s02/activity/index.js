/* ADVANCED JAVASCRIPT OOP - POLYMORPHISM (ACTIVITY) */

/* ACTIVITY 1 */
// Base class
class Equipment {
	constructor(equipmentType) {
		this.equipmentType = equipmentType;
	}
	printInfo() {
		return `Info: ${this.equipmentType}`
	}
}
// Subclasses
class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType) {
		super(equipmentType);
		this.model = model;
		this.bladeType = bladeType;
	}
	printInfo() {
		return super.printInfo() + "\n" + `The ${this.equipmentType} ${this.model} has a ${this.bladeType}.`;
	}
}
let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment {
	constructor(equipmentType, model, hookRadius, maxCapacity) {
		super(equipmentType);
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}
	printInfo() {
		return super.printInfo() + "\n" + `The ${this.equipmentType} ${this.model} has ${this.hookRadius} hook radius and ${this.maxCapacity} max capacity.`;
	}
}
let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towerCrane1.printInfo());

class BackLoader extends Equipment {
	constructor(equipmentType, model, type, tippingLoad) {
		super(equipmentType);
		this.model = model;
		this.type = type;
		this.tippingLoad = tippingLoad;
	}
	printInfo() {
		return super.printInfo() + "\n" + `The ${this.equipmentType} ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad} lbs.`;
	}
}
let backLoader1 = new BackLoader("back loader", "Turtle", "hydraulic", 1500);
console.log(backLoader1.printInfo());


/* ACTIVITY 2 */
// Base class
class RegularShape {
	constructor(noSides, length) {
		this.noSides = noSides;
		this.length = length;
	}
	getPerimeter() {
		return "The perimeter is ";
	}
	getArea() {
		return "The area is ";
	}
}
// Subclasses
class Triangle extends RegularShape {
	constructor(noSides, length) {
		super(noSides, length);
	}
	getPerimeter() {
		if (this.noSides === 3) {
		    return super.getPerimeter() + " " + (this.noSides * this.length) + ".";
		} else {
			return "Invalid input! A triangle has 3 sides.";
		}
	}
	getArea() {
		if (this.noSides === 3) {
		    return super.getArea() + " " + (Math.sqrt(3) / 4) * Math.pow(this.length, 2) + ".";
		} else {
			return "Invalid input! A triangle has 3 sides.";
		}
	}
}
let triangle1 = new Triangle(3, 10);
console.log(triangle1.getPerimeter());
console.log(triangle1.getArea());

class Square extends RegularShape {
	constructor(noSides, length) {
		super(noSides, length);
	}
	getPerimeter() {
		if (this.noSides === 4) {
		    return super.getPerimeter() + " " + (this.noSides * this.length) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
	getArea() {
		if (this.noSides === 4) {
		    return super.getArea() + " " + (this.length * this.length) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
}
let square1 = new Square(4, 12);
console.log(square1.getPerimeter());
console.log(square1.getArea());
