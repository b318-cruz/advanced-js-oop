/* ADVANCED JAVASCRIPT OOP - ABSTRACTION (ACTIVITY) */

/* ACTIVITY 1 */
// 1st base class
class RegularShape {
	constructor(noSides, length) {
		if(this.constructor === RegularShape) {
			throw new Error(
				"You are not allowed to create an object using the RegularShape class."
			);
		}
		this.noSides = noSides;
		this.length = length;

		if(this.getPerimeter === undefined) {
			throw new Error(
				"Class must implement 'getPerimeter()' method."
			);
		}

		if(this.getArea === undefined) {
			throw new Error(
				"Class must implement 'getArea()' method."
			);
		}
	}
}

// 1st subclasses
class Square extends RegularShape {
	constructor(noSides, length) {
		super(noSides, length);
	}
	getPerimeter() {
		if (this.noSides === 4) {
		   return "The perimeter is " + (this.noSides * this.length) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
	getArea() {
		if (this.noSides === 4) {
		    return "The area is " + (this.length * this.length) + ".";
		} else {
			return "Invalid input! A square has 4 sides.";
		}
	}
}
let shape1 = new Square(4, 16);
console.log(shape1.getPerimeter());
console.log(shape1.getArea());



/* ACTIVITY 2 */
// 2nd base class
class Food {
	constructor(name, price) {
		if(this.constructor === Food) {
			throw new Error(
				"You are not allowed to create an object using the Food class."
			);
		}
		this.name = name;
		this.price = price;

		if(this.getName === undefined) {
			throw new Error(
				"Class must implement 'getName()' method."
			);
		}
	}
}

// 2nd subclass
class Vegetable extends Food {
	constructor(name, breed, price) {
		super(name, price);
		this.breed = breed;
	}

	getName() {
		return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`;
	}
}
const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName());



/* ACTIVITY 3 */
// 3rd base class
class Equipment {
	constructor(equipmentType, model) {
		if(this.constructor === Equipment) {
			throw new Error(
				"You are not allowed to create an object using the Equipment class."
			);
		}
		this.equipmentType = equipmentType;
		this.model = model;

		if(this.printInfo === undefined) {
			throw new Error(
				"Class must implement 'printInfo()' method."
			);
		}
	}
}

// 3rd subclasses
class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType) {
		super(equipmentType, model);
		this.bladeType = bladeType;
	}
	printInfo() {
		return `Info: ${this.equipmentType}` + "\n" + `The ${this.equipmentType} ${this.model} has a ${this.bladeType} blade.`;
	}
}
let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment {
	constructor(equipmentType, model, hookRadius, maxCapacity) {
		super(equipmentType, model);
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}
	printInfo() {
		return `Info: ${this.equipmentType}` + "\n" + `The ${this.equipmentType} ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} max capacity.`;
	}
}
let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towerCrane1.printInfo());