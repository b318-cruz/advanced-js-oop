/* ADVANCED JAVASCRIPT OOP - ABSTRACTION */

// What is Abstraction?
/*
	It is hiding of unnecessary details from a user in order to manage the complexity of a system.

	In OOP, this translates to just knowing which object methods can be called and what parameters to provide them. The implementation that leads to the expected output is hidden away.
*/


/*
	Abstraction is like creating a simplified model or blueprint. It helps us focus on what's important and ignore the complicated details.
*/
// This class will serve as a blueprint for Employee Class
class Person {
	constructor() {
		if(this.constructor === Person) {
			throw new Error(
				"Object cannot be created from an abstract class Person."
			);
		}
		// Method for Error in Constructors
		/*
			constructor(firstName, lastName) {
				if(this.constructor === Person) {
					throw new Error(
						"You are not allowed to create an object using the Person class."
					);
					this.firstName = firstName;
					this.lastName = lastName;
				}
			}
		*/

		if(this.getFullName === undefined) {
			throw new Error(
				"Class must implement getFullName() method."
			);
		}

		if(this.getFirstName === undefined) {
			throw new Error(
				"Class must implement getFirstName() method."
			);
		}

		if(this.getLastName === undefined) {
			throw new Error(
				"Class must implement getLastName() method."
			);
		}
	}
}

// Subclasses
class Employee extends Person {
	constructor(firstName, lastName, employeeID) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeID = employeeID;
	}

	getFullName() {
		return `${this.firstName} ${this.lastName} has an employee ID of ${this.employeeID}.`;
	}

	getFirstName() {
		return `First Name: ${this.firstName}`;
	}

	getLastName() {
		return `Last Name: ${this.lastName}`;
	}
}
const employeeA = new Employee('John', 'Smith', 'EM-004');
console.log(employeeA.getFirstName());
console.log(employeeA.getLastName());
console.log(employeeA.getFullName());

class Student extends Person {
	constructor(firstName, lastName, studentID, section) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentID = studentID;
		this.section = section;
	}

	getFullName() {
		return `${this.firstName} ${this.lastName} has a student ID of ${this.studentID}.`;
	}

	getFirstName() {
		return `First Name: ${this.firstName}`;
	}

	getLastName() {
		return `Last Name: ${this.lastName}`;
	}

	// Additional method (allowed)
	getStudentDetails() {
		return `${this.firstName} ${this.lastName} has a student ID of ${this.studentID} and belongs to the ${this.section} section.`;
	}
}
const student1 = new Student('Brandon', 'Boyd', 'SID-1100', 'Mango');
const student2 = new Student('Jeff', 'Cruz', 'SID-1101', 'Narra');
const student3 = new Student('Jane', 'Doe', 'SID-1102', 'Sampaguita');
console.log(student1.getStudentDetails());
console.log(student2.getStudentDetails());
console.log(student3.getStudentDetails());